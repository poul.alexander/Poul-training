* Basic dispatch model including 3 power plants, 2 exogenous power generators, within one power market

SETS
    sGen         dispatchable generating assets
         /GAS1, GAS2, GAS3, COAL1/
    sRes         exogenous generating plants
              /NUC1  A nuclear plant
               SOL1  A solar plant
               SOL2  Another solar plant/
    sStore       storage assets (e.g. battery)
         /BATT1, BATT2/
    sGenLim      min and max limits for generating assets (MWh)
         /MIN, MAX/
    sReg         set of regions
         /REG1, REG2/
    sIC          interconnectors
         /IC1/
    sT           time (hours)
         /0*23/

ALIAS (sReg,aReg);

SETS
    sGenByRegion(sReg,sGen)      mapping of generators to regions
         /REG1.GAS1, REG1.GAS2, REG1.COAL1, REG2.GAS3/
    sResByRegion(sReg,sRes)      mapping of residual generators to regions
         /REG1.NUC1, REG1.SOL1, REG2.SOL2/
    sStoreByRegion(sReg,sStore)  mapping of stores to regions
         /REG1.BATT1, REG2.BATT2/

TABLE tCapacity(sGen,sGenLim)    Capacity limits of the dispatchable assets
                 MIN     MAX
         GAS1    0       2
         GAS2    0       3
         GAS3    0       4
         COAL1   0       2

TABLE tStoreCap(sStore,sGenLim)  Capacity Limits of storage plants
                 MIN     MAX
         BATT1   0       1
         BATT2   0       0.5

TABLE tStoreSupply(sStore,sGenLim)  Capacity Limits of storage plants
                 MIN     MAX
         BATT1   0       0.5
         BATT2   0       0.25

TABLE tICCap(sReg,aReg,sGenLim)         Capacity limits of interconnectors
                         MIN     MAX
         REG1.REG2       0       0.25
         REG2.REG1       0       0.5

TABLE tResGen(sT,sRes)           Generation of the exogenous plants
                 NUC1    SOL1    SOL2
         0       2       0       0
         1       2       0       0
         2       2       0       0
         3       2       0       0
         4       2       0       0
         5       2       0       0
         6       2       0.1     0.2
         7       2       0.3     0.6
         8       2       0.5     0.9
         9       2       0.4     0.7
         10      2       0.6     1.2
         11      2       0.8     1.5
         12      2       1       1.5
         13      1.9     1       1.5
         14      1.8     1       1.5
         15      1.6     0.6     1.0
         16      1.4     0.4     0.8
         17      1.2     0.5     1.0
         18      1.1     0.3     0.6
         19      1       0.2     0.2
         20      1       0       0
         21      1       0       0
         22      1       0       0
         23      1       0       0;

TABLE tDem(sT, sReg)         demand (MWh)
                 REG1    REG2
         0       1.4     0.1
         1       1.3     0.2
         2       1.5     0.4
         3       1.8     0.1
         4       2       0.3
         5       2.5     0.8
         6       3.2     1.2
         7       5.4     2.0
         8       6       3.1
         9       6.5     4.2
         10      5.5     5.2
         11      5.2     4.8
         12      5.3     5.1
         13      6.0     5.3
         14      5.6     5.4
         15      4.3     5.5
         16      3.7     5.2
         17      5.2     4.9
         18      4.6     5.0
         19      2.3     2.4
         20      4.5     1.3
         21      3.0     0.2
         22      2.1     0.1
         23      1.7     0.2;

PARAMETERS
pOpCost(sGen) operating costs for each plant (fixed per asset)
         /GAS1   65
          GAS2   40
          GAS3   55
          COAL1  20/

pFuelCost(sGen) fueling costs per MWh per plant (?)
         /GAS1    25
          GAS2    25
          GAS3    30
          COAL1   45/

pGenRamp(sGen) maximum change in output (MWh) per plant
         /GAS1    0.5
          GAS2    0.25
          GAS3    0.5
          COAL1   1/

pGenRampCost(sGen) charge for change in output (MWh) per plant
         /GAS1    1
          GAS2    1
          GAS3    1
          COAL1   2/

pStoreCost(sStore) operating costs for each storage asset
         /BATT1  10
          BATT2  10/

POSITIVE VARIABLES
xLoad(sGen,sT)           load per asset per hour (MWh)
xRampUp(sGen,sT)         change in ouput between T and T+1
xRampDown(sGen,sT)       change in ouput between T and T+1
xStore(sStore,sT)        charge in each asset per hour
xStoreCharge(sStore,sT)  charge from each storage asset per hour
xStoreSupply(sStore,sT)  discharge from each storage asset per hour
xDisCost(sT,sReg)        costs per asset per hour (?)
xSpill(sT,sReg)          spill cost (regional)
xLoL(sT,sReg)            loss of load (regional)
xIC_fromto(sReg,aReg,sT)  ntc flows from regions sReg to aReg


VARIABLES
xRegCost(sT)             regional cost of generation per day
xTotCost                 total value of generation per day;

EQUATIONS
eLoad(sT,sReg)           market clearing equation - ensure total dispatch generation supplies grid. Ramping is linear
eRamp(sGen,sT)           output change equation (ramping)
eStore(sStore,sT)        charge stored in each storage asset per hour
eDisCost(sT,sReg)        cost per hour
eRegCost(sT)             regional cost
eTotCost                 total cost
;

eLoad(sT,sReg)..         SUM(sGenByRegion(sReg,sGen),xLoad(sGen,sT) + xRampUp(sGen,sT)/2 - xRampDown(sGen,sT)/2)
                         + SUM(sStoreByRegion(sReg,sStore), xStoreSupply(sStore,sT))
                         + SUM(aReg$(not SameAs(aReg,sReg)),xIC_fromto(aReg,sReg,sT))
                         + xLol(sT,sReg)
                         =E=
                         tDem(sT,sReg) - SUM(sResByRegion(sReg,sRes),tResGen(sT,sRes))
                         + SUM(sStoreByRegion(sReg,sStore), xStoreCharge(sStore,sT))
                         + SUM(aReg$(not SameAs(aReg,sReg)),xIC_fromto(sReg,aReg,sT))
                         + xSpill(sT,sReg);
eRamp(sGen,sT)..         xLoad(sGen,sT+1) =E= xLoad(sGen,sT) + xRampUp(sGen,sT) - xRampDown(sGen,sT);
eStore(sStore,sT)..      xStore(sStore,sT+1) =E= xStore(sStore,sT) + xStoreCharge(sStore,sT) - xStoreSupply(sStore,sT);
eDisCost(sT,sReg)..      xDisCost(sT,sReg)
                         =E= SUM(SGenByRegion(sReg,sGen), xLoad(sGen,sT) * pFuelCost(sGen)
                         + (xRampUp(sGen,sT) + xRampDown(sGen,sT)) * pGenRampCost(sGen)
                         + pOpCost(sGen));
eRegCost(sT)..           xRegCost(sT) =E= SUM(sReg, xDisCost(sT,sReg) + xLoL(sT,sReg) * 10000 + xSpill(sT,sReg) * 5);
eTotCost..               xTotCost =E= SUM(sT,xRegCost(sT));

*Set upper limits of capacity + storage
xStore.UP(sStore,sT) = tStoreCap(sStore,"MAX");
xLoad.UP(sGen,sT) = tCapacity(sGen,"MAX");
xIC_fromto.up(sReg,aReg,sT) = tICCap(sReg,aReg,"MAX");

*Set upper limits of output/input capacity & ramping for each source
xStoreCharge.UP(sStore,sT) = tStoreSupply(sStore,"MAX");
xStoreSupply.UP(sStore,sT) = tStoreSupply(sStore,"MAX");
xRampUp.UP(sGen,sT) = pGenRamp(sGen);
xRampDown.UP(sGen,sT) = pGenRamp(sGen);

MODEL DISPMODEL  /All/;
SOLVE DISPMODEL USING LP MINIMIZE xTotCost;

PARAMETER Load Generation from each Asset;
Load(sT, sGen) = xLoad.L(sGen,sT);

PARAMETER Cost Cost of running each asset (includes op costs);
Cost(sT,sGen) = xLoad.L(sGen,sT)*pFuelCost(sGen)
                 + xRampUp.L(sGen,sT) * pGenRampCost(sGen)
                 + xRampDown.L(sGen,sT) * pGenRampCost(sGen)
                 + pOpCost(sGen);

PARAMETER Supply Demand and supply per region;
Supply(sT,sReg,"Demand") = -tDem(sT, sReg);
Supply(sT,sReg,"Exo. Gen") = SUM(sResByRegion(sReg,sRes),tResGen(sT,sRes));
Supply(sT,sReg,"Disp. Gen") = SUM(sGenByRegion(sReg,sGen),xLoad.L(sGen,sT) + xRampUp.L(sGen,sT)/2 - xRampDown.L(sGen,sT)/2);
Supply(sT,sReg,"IC") = SUM(aReg$(not SameAs(aReg,sReg)),xIC_fromto.L(aReg,sReg,sT)) - SUM(aReg$(not SameAs(aReg,sReg)),xIC_fromto.L(sReg,aReg,sT))
OPTION Supply:3:1:2;

PARAMETER Battery Behaviour of batteries;
Battery(sT,sStore,"Energy") =  xStore.L(sStore, sT);
Battery(sT,sStore,"Charge/Discharge") =  xStoreCharge.L(sStore,sT) - xStoreSupply.L(sStore, sT);
OPTION Battery:3:1:2;

DISPLAY Load, Cost, Supply, Battery;
